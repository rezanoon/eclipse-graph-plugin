package org.eclipse.plugins.jdtgraph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CreateGraphAction implements IWorkbenchWindowActionDelegate {
	IWorkbenchWindow activeWindow = null;

	@Override
	public void run(IAction action) {
		ITextSelection selected = getCurrentSelection();
		String response = "didn't get anything";
		try {
			response = sendPost(mainpackage.Main.createJsonComplete(selected.getText()));

			showResultView(ResultView.ID, createJsonResponse(response));

		} catch (IOException | ParseException e) {
			showResultView(ResultView.ID, "something wrong with server results");
			e.printStackTrace();
		}

	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void init(IWorkbenchWindow window) {
		activeWindow = window;
	}

	public ITextSelection getCurrentSelection() {
		IEditorPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (part instanceof ITextEditor) {
			final ITextEditor editor = (ITextEditor) part;
			ISelection sel = editor.getSelectionProvider().getSelection();
			if (sel instanceof TextSelection) {
				ITextSelection textSel = (ITextSelection) sel;
				return textSel;
			}
		}
		return null;
	}

	private String sendPost(String str) throws IOException {

		String url = "http://127.0.0.1:8000/blog/home";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("content-type", "text/plain");

		// Send post request
		con.setDoOutput(true);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
		bw.write(str);
		bw.flush();

		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());
		return response.toString();
	}

	private JSONArray createJsonResponse(String response) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONArray json = (JSONArray) parser.parse(response);
		return json;
	}

	private void showResultView(String id, JSONArray array) {
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(id);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		ResultView part = (ResultView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.findView(id);
		part.getnameUrlMap().clear();

		if (!array.isEmpty()) {
			ArrayList<String> names = new ArrayList<String>();
			for (int i = 0; i < array.size(); i++) {
				JSONObject json = (JSONObject) array.get(i);
				part.addToNameUrlMap((String) json.get("name"), (String) json.get("url"));
				names.add((String) json.get("name"));
			}
			part.setViewerString(names.toArray(new String[] {}));
		} else {
			showResultView(id, "nothing found");
		}
	}

	private void showResultView(String id, String oneItem) {
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(id);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		ResultView part = (ResultView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
				.findView(id);
		part.getnameUrlMap().clear();
		part.setViewerString(new String[] { oneItem });
	}

}
